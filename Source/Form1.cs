﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vruler
{
    public partial class Form_Vruler : Form
    {
        //Declare instances of length class
        private Length firstLength = new Length();
        private Length secondLength = new Length();

        String[] names = { "Text_Feet1", "Text_Inch1", "Text_Half1", "Text_Quarter1", "Text_Eighth1", "Text_16th1", 
                           "Text_Feet2", "Text_Inch2", "Text_Half2", "Text_Quarter2", "Text_Eighth2", "Text_16th2" };
        
        public Form_Vruler()
        {
            InitializeComponent();
        }

        private void TextboxChange(object sender, EventArgs e)
        {
            //Declare integer for loops and indexing
            int i = 0;
            //Declare output variable
            float value;
            //Get textbox that was changed
            TextBox text = (TextBox)sender;
            //Convert textbox name to shorter tag
            string tag = text.Name.Substring(5, text.Name.Length - 5);
            //Find textbox name in list and set index number
            for (i = 0; i < 12; i++) { if (names[i] == text.Name) { break; } }
            //Try parsing text in textbox to float value
            if (float.TryParse(text.Text, out value))
            {
                //Call ch_attr for different class instance based on row and index number
                if (text.Name.EndsWith("1")) { firstLength.ch_attr(i, value); }
                else if (text.Name.EndsWith("2")) { secondLength.ch_attr(i, value); }
                //Reset warning label
                Label_Warning.Text = " ";
            }
            //If the text box is empty or was deleted
            else if (string.IsNullOrEmpty(text.Text))
            {
                //Set attribute value back to zero
                if (text.Name.EndsWith("1")) { firstLength.ch_attr(i, 0); }
                else if (text.Name.EndsWith("2")) { secondLength.ch_attr(i, 0); }
                //Reset warning label
                Label_Warning.Text = " ";
            }
            //If the text entered is a non-numerical character
            else
            {
                //warning to user, invalid character (Non-Numerical)
                Label_Warning.Text = "Invalid Character Entered (" + tag + ")";
            }

            //Calculate overall lengths 1 and 2
            float fl = firstLength.getLength();
            float sl = secondLength.getLength();
            //Calculate rounded up int to check for 0 value
            int flr = Convert.ToInt32(Math.Ceiling((fl)));
            int slr = Convert.ToInt32(Math.Ceiling((sl)));
            //Check results and change output + label text accordingly
            //Empty
            if (flr == 0 & slr == 0)
            {
                Label_Result.Text = "";
                Text_Result.Text = "";
            }
            //FirstLength only
            else if (flr > 0 & slr == 0)
            {
                Label_Result.Text = "Length:";
                Text_Result.Text = Convert.ToString(fl);
            }
            //SecondLength only
            else if (flr == 0 & slr > 0)
            {
                Label_Result.Text = "Length:";
                Text_Result.Text = Convert.ToString(sl);
            }
            //Both
            else if (flr > 0 & slr > 0)
            {
                //Change Label to Area
                Label_Result.Text = "   Area:";
                //Change output to product of lengths
                Text_Result.Text = Convert.ToString(fl * sl);
            }
        }

        private void Handle_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Get textbox name
            TextBox text = (TextBox)sender;
            //Check if the key pressed was the "Return" key (CALC/COPY)
            if (e.KeyChar == (char)Keys.Return)
            {
                //Check if already focused on Result textbox
                if (text.Name == "Text_Result")
                {
                    //If on Results check that there is a value
                    if (!String.IsNullOrEmpty(Text_Result.Text))
                    {
                        //If there is a value copy to clipboard and change label
                        Clipboard.SetText(Text_Result.Text);
                        Label_Warning.Text = "Result copied to clipboard";
                    }
                    //If there is no value do nothing
                }
                else
                {
                    //If not change focus to Result textbox
                    Text_Result.Select();
                }
                //Always close event
                e.Handled = true;
            }
            //Check if the key pressed was the "Esc" key (CLEAR DATA)
            if (e.KeyChar == (char)Keys.Escape)
            {
                //Reset First Row
                Text_Feet1.Text = null; firstLength.feet = 0;
                Text_Inch1.Text = null; firstLength.inches = 0;
                Text_Half1.Text = null; firstLength.halfinches = 0;
                Text_Quarter1.Text = null; firstLength.quarterinches = 0;
                Text_Eighth1.Text = null; firstLength.eighthinches = 0;
                Text_16th1.Text = null; firstLength.sixteenthinches = 0;
                //Reset Second Row
                Text_Feet2.Text = null; secondLength.feet = 0;
                Text_Inch2.Text = null; secondLength.inches = 0;
                Text_Half2.Text = null; secondLength.halfinches = 0;
                Text_Quarter2.Text = null; secondLength.quarterinches = 0;
                Text_Eighth2.Text = null; secondLength.eighthinches = 0;
                Text_16th2.Text = null; secondLength.sixteenthinches = 0;
                //Reset result box and change label
                Text_Result.Text = null;
                Label_Warning.Text = "Vaules Reset";
                //Select first box
                Text_Feet1.Select();
                //Close event
                e.Handled = true;
            }
        }
    }

}


