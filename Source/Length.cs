﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vruler
{
    class Length
    {
        public float feet;
        public float inches;
        public float halfinches;
        public float quarterinches;
        public float eighthinches;
        public float sixteenthinches;
        public void ch_attr(int index, float value)
        {
            switch (index)
            {
                case 0: case 6: feet = value; break;
                case 1: case 7: inches = value; break;
                case 2: case 8: halfinches = value; break;
                case 3: case 9: quarterinches = value; break;
                case 4: case 10: eighthinches = value; break;
                case 5: case 11: sixteenthinches = value; break;
            }
        }
        public float getLength()
        {
            return feet + (inches/12) + (halfinches/24) + (quarterinches/48) + (eighthinches/96) + (sixteenthinches/192);
        }
    } 
}
