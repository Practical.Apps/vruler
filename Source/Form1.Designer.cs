﻿namespace Vruler
{
    partial class Form_Vruler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Vruler));
            this.Text_Feet1 = new System.Windows.Forms.TextBox();
            this.Text_Inch1 = new System.Windows.Forms.TextBox();
            this.Text_Half1 = new System.Windows.Forms.TextBox();
            this.Text_Quarter1 = new System.Windows.Forms.TextBox();
            this.Text_Eighth1 = new System.Windows.Forms.TextBox();
            this.Text_16th1 = new System.Windows.Forms.TextBox();
            this.Label_Feet = new System.Windows.Forms.Label();
            this.Label_In = new System.Windows.Forms.Label();
            this.Label_Half = new System.Windows.Forms.Label();
            this.Label_Quarter = new System.Windows.Forms.Label();
            this.Label_Eighth = new System.Windows.Forms.Label();
            this.Label_Sixteenth = new System.Windows.Forms.Label();
            this.Text_16th2 = new System.Windows.Forms.TextBox();
            this.Text_Eighth2 = new System.Windows.Forms.TextBox();
            this.Text_Quarter2 = new System.Windows.Forms.TextBox();
            this.Text_Half2 = new System.Windows.Forms.TextBox();
            this.Text_Inch2 = new System.Windows.Forms.TextBox();
            this.Text_Feet2 = new System.Windows.Forms.TextBox();
            this.Text_Result = new System.Windows.Forms.TextBox();
            this.Label_Result = new System.Windows.Forms.Label();
            this.Label_Warning = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Text_Feet1
            // 
            this.Text_Feet1.Location = new System.Drawing.Point(33, 27);
            this.Text_Feet1.Name = "Text_Feet1";
            this.Text_Feet1.Size = new System.Drawing.Size(45, 20);
            this.Text_Feet1.TabIndex = 0;
            this.Text_Feet1.Tag = "";
            this.Text_Feet1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Feet1.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Inch1
            // 
            this.Text_Inch1.Location = new System.Drawing.Point(119, 27);
            this.Text_Inch1.Name = "Text_Inch1";
            this.Text_Inch1.Size = new System.Drawing.Size(45, 20);
            this.Text_Inch1.TabIndex = 1;
            this.Text_Inch1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Inch1.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Half1
            // 
            this.Text_Half1.Location = new System.Drawing.Point(199, 27);
            this.Text_Half1.Name = "Text_Half1";
            this.Text_Half1.Size = new System.Drawing.Size(30, 20);
            this.Text_Half1.TabIndex = 2;
            this.Text_Half1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Half1.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Quarter1
            // 
            this.Text_Quarter1.Location = new System.Drawing.Point(244, 27);
            this.Text_Quarter1.Name = "Text_Quarter1";
            this.Text_Quarter1.Size = new System.Drawing.Size(30, 20);
            this.Text_Quarter1.TabIndex = 3;
            this.Text_Quarter1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Quarter1.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Eighth1
            // 
            this.Text_Eighth1.Location = new System.Drawing.Point(290, 27);
            this.Text_Eighth1.Name = "Text_Eighth1";
            this.Text_Eighth1.Size = new System.Drawing.Size(30, 20);
            this.Text_Eighth1.TabIndex = 4;
            this.Text_Eighth1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Eighth1.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_16th1
            // 
            this.Text_16th1.Location = new System.Drawing.Point(336, 27);
            this.Text_16th1.Name = "Text_16th1";
            this.Text_16th1.Size = new System.Drawing.Size(30, 20);
            this.Text_16th1.TabIndex = 5;
            this.Text_16th1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_16th1.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Label_Feet
            // 
            this.Label_Feet.AutoSize = true;
            this.Label_Feet.Location = new System.Drawing.Point(8, 30);
            this.Label_Feet.Name = "Label_Feet";
            this.Label_Feet.Size = new System.Drawing.Size(19, 13);
            this.Label_Feet.TabIndex = 6;
            this.Label_Feet.Text = "Ft:";
            // 
            // Label_In
            // 
            this.Label_In.AutoSize = true;
            this.Label_In.Location = new System.Drawing.Point(94, 30);
            this.Label_In.Name = "Label_In";
            this.Label_In.Size = new System.Drawing.Size(19, 13);
            this.Label_In.TabIndex = 7;
            this.Label_In.Text = "In:";
            // 
            // Label_Half
            // 
            this.Label_Half.AutoSize = true;
            this.Label_Half.Location = new System.Drawing.Point(201, 11);
            this.Label_Half.Name = "Label_Half";
            this.Label_Half.Size = new System.Drawing.Size(24, 13);
            this.Label_Half.TabIndex = 8;
            this.Label_Half.Text = "1/2";
            // 
            // Label_Quarter
            // 
            this.Label_Quarter.AutoSize = true;
            this.Label_Quarter.Location = new System.Drawing.Point(246, 11);
            this.Label_Quarter.Name = "Label_Quarter";
            this.Label_Quarter.Size = new System.Drawing.Size(24, 13);
            this.Label_Quarter.TabIndex = 9;
            this.Label_Quarter.Text = "1/4";
            // 
            // Label_Eighth
            // 
            this.Label_Eighth.AutoSize = true;
            this.Label_Eighth.Location = new System.Drawing.Point(292, 11);
            this.Label_Eighth.Name = "Label_Eighth";
            this.Label_Eighth.Size = new System.Drawing.Size(24, 13);
            this.Label_Eighth.TabIndex = 10;
            this.Label_Eighth.Text = "1/8";
            // 
            // Label_Sixteenth
            // 
            this.Label_Sixteenth.AutoSize = true;
            this.Label_Sixteenth.Location = new System.Drawing.Point(335, 11);
            this.Label_Sixteenth.Name = "Label_Sixteenth";
            this.Label_Sixteenth.Size = new System.Drawing.Size(30, 13);
            this.Label_Sixteenth.TabIndex = 11;
            this.Label_Sixteenth.Text = "1/16";
            // 
            // Text_16th2
            // 
            this.Text_16th2.Location = new System.Drawing.Point(336, 53);
            this.Text_16th2.Name = "Text_16th2";
            this.Text_16th2.Size = new System.Drawing.Size(30, 20);
            this.Text_16th2.TabIndex = 17;
            this.Text_16th2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_16th2.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Eighth2
            // 
            this.Text_Eighth2.Location = new System.Drawing.Point(290, 53);
            this.Text_Eighth2.Name = "Text_Eighth2";
            this.Text_Eighth2.Size = new System.Drawing.Size(30, 20);
            this.Text_Eighth2.TabIndex = 16;
            this.Text_Eighth2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Eighth2.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Quarter2
            // 
            this.Text_Quarter2.Location = new System.Drawing.Point(244, 53);
            this.Text_Quarter2.Name = "Text_Quarter2";
            this.Text_Quarter2.Size = new System.Drawing.Size(30, 20);
            this.Text_Quarter2.TabIndex = 15;
            this.Text_Quarter2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Quarter2.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Half2
            // 
            this.Text_Half2.Location = new System.Drawing.Point(199, 53);
            this.Text_Half2.Name = "Text_Half2";
            this.Text_Half2.Size = new System.Drawing.Size(30, 20);
            this.Text_Half2.TabIndex = 14;
            this.Text_Half2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Half2.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Inch2
            // 
            this.Text_Inch2.Location = new System.Drawing.Point(119, 53);
            this.Text_Inch2.Name = "Text_Inch2";
            this.Text_Inch2.Size = new System.Drawing.Size(45, 20);
            this.Text_Inch2.TabIndex = 13;
            this.Text_Inch2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Inch2.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Feet2
            // 
            this.Text_Feet2.Location = new System.Drawing.Point(33, 53);
            this.Text_Feet2.Name = "Text_Feet2";
            this.Text_Feet2.Size = new System.Drawing.Size(45, 20);
            this.Text_Feet2.TabIndex = 12;
            this.Text_Feet2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            this.Text_Feet2.Leave += new System.EventHandler(this.TextboxChange);
            // 
            // Text_Result
            // 
            this.Text_Result.Location = new System.Drawing.Point(251, 79);
            this.Text_Result.Name = "Text_Result";
            this.Text_Result.Size = new System.Drawing.Size(115, 20);
            this.Text_Result.TabIndex = 18;
            this.Text_Result.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Handle_KeyPress);
            // 
            // Label_Result
            // 
            this.Label_Result.AutoSize = true;
            this.Label_Result.Location = new System.Drawing.Point(210, 82);
            this.Label_Result.Name = "Label_Result";
            this.Label_Result.Size = new System.Drawing.Size(43, 13);
            this.Label_Result.TabIndex = 19;
            this.Label_Result.Text = "Length:";
            // 
            // Label_Warning
            // 
            this.Label_Warning.AutoSize = true;
            this.Label_Warning.Location = new System.Drawing.Point(30, 82);
            this.Label_Warning.Name = "Label_Warning";
            this.Label_Warning.Size = new System.Drawing.Size(0, 13);
            this.Label_Warning.TabIndex = 20;
            // 
            // Form_Vruler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 114);
            this.Controls.Add(this.Label_Warning);
            this.Controls.Add(this.Label_Result);
            this.Controls.Add(this.Text_Result);
            this.Controls.Add(this.Text_16th2);
            this.Controls.Add(this.Text_Eighth2);
            this.Controls.Add(this.Text_Quarter2);
            this.Controls.Add(this.Text_Half2);
            this.Controls.Add(this.Text_Inch2);
            this.Controls.Add(this.Text_Feet2);
            this.Controls.Add(this.Label_Sixteenth);
            this.Controls.Add(this.Label_Eighth);
            this.Controls.Add(this.Label_Quarter);
            this.Controls.Add(this.Label_Half);
            this.Controls.Add(this.Label_In);
            this.Controls.Add(this.Label_Feet);
            this.Controls.Add(this.Text_16th1);
            this.Controls.Add(this.Text_Eighth1);
            this.Controls.Add(this.Text_Quarter1);
            this.Controls.Add(this.Text_Half1);
            this.Controls.Add(this.Text_Inch1);
            this.Controls.Add(this.Text_Feet1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Vruler";
            this.Text = "Vruler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Text_Feet1;
        private System.Windows.Forms.TextBox Text_Inch1;
        private System.Windows.Forms.TextBox Text_Half1;
        private System.Windows.Forms.TextBox Text_Quarter1;
        private System.Windows.Forms.TextBox Text_Eighth1;
        private System.Windows.Forms.TextBox Text_16th1;
        private System.Windows.Forms.Label Label_Feet;
        private System.Windows.Forms.Label Label_In;
        private System.Windows.Forms.Label Label_Half;
        private System.Windows.Forms.Label Label_Quarter;
        private System.Windows.Forms.Label Label_Eighth;
        private System.Windows.Forms.Label Label_Sixteenth;
        private System.Windows.Forms.TextBox Text_16th2;
        private System.Windows.Forms.TextBox Text_Eighth2;
        private System.Windows.Forms.TextBox Text_Quarter2;
        private System.Windows.Forms.TextBox Text_Half2;
        private System.Windows.Forms.TextBox Text_Inch2;
        private System.Windows.Forms.TextBox Text_Feet2;
        private System.Windows.Forms.TextBox Text_Result;
        private System.Windows.Forms.Label Label_Result;
        private System.Windows.Forms.Label Label_Warning;
    }
}

