**Vruler 1.0**

**Summary**\
This program is a calculator for lengths in imperial units (Feet/Inches/Fractionals) and areas based on those lengths.

**Why**\
Years ago I worked at a company which determined energy efficiency ratings for new home constructions. A significant factor in the rating was the surface area of windows to the outside.\
Windows are made to obscure dimensions more often than you'd think and this includes fractionals down to the 16th of an inch, calculating these dimensions became fairly time consuming.\
Not only that but often one dimension persists between windows which leads to more time wasted using a traditional calculator. This custom calculator widget ended up being the solution.

**How**\
Operation is simple and straight forward, enter values into the corresponding text boxes and as you change focus from one text box to another (click/tab/enter) the result box updates with a decimal length in feet. As soon as you put a value in any of the other row of boxes the result will change to calculate area as a product of the two dimensions (Decimal, Square Feet). Pressing "Enter" at any time will take you to the result box. Pressing "Enter" again while focus is on the result box will copy the result value inside to the clipboard. Pressing "Esc" at any time will reset all values and return your cursor to the first box.

**Checksum**\
MD5..........Vruler.exe.....4c2e3d3f5ce2f923c5705cc5c71ad7cd\
SHA256.....Vruler.exe.....d7c4829ebe9438aa03fcfd3e043ef69a580c8372326be8e446380ed7a8929c67

**License**\
I have made this program available to you under the GPL V2 license.\
You are free to use the program as you choose but any distribution of the program/source or modifications made to it must abide by restrictions of the license.